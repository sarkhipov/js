/**
 * @file This library help in work with audio-sprites. Packaged in module {@link module:audio-sprites}
 * @author sarkhipov
 */

/**
 * <h2>Usage examples</h2>
 * <h4>Basics</h4>
 * <p>For use this library you need already loaded audio object. Lets it will be variable 'audio.'</p>
 * <p>You need to create new audio element with library. Get to the function audio object and set of sprites:</p>
 * <pre>
 * var audioElement = as.create(audio, {
 *      one: {start: 0.1, stop: 1.1},
 *      two: {start: 1.2, stop: 2.1}
 * });
 * </pre>
 * <p><strong>Note: </strong> one audio object - one audio element with one set of sprites!</p>
 * <p>Then play any sprite with method 'play'. It will be played random sprite, if you will not pass argument:</p>
 * <pre>
 * audioElement.play('one');
 * </pre>
 * <h4>Pause callback</h4>
 * <p>You can handle sprite pause event with callback function in play method:</p>
 * <pre>
 * audioElement.play('two', function() {
 *      console.log('second sprite stoped');
 * });
 * </pre>
 * <h4>Loop sprite</h4>
 * <p>You can loop current sprite if you need. Looping will stop with another sprite playing, or with 'stop' method.</p>
 * <pre>
 * audioElement.play('two', function() {
 *      console.log('second sprite played. play again');
 * }, true);
 * </pre>
 * @module audio-sprites
 */

/**
 * @namespace
 */
var as = {
    // avaliable audio sprites
    sprites: [],
    // current sprite
    current: null,
    // audio object
    audio: null,
    // is elements declared
    loaded: false,

    /**
     * Create new audio element
     * @param audio {object} Audio object with main audio file
     * @param sprites {object} Set of sprites.
     * @returns {as.audio_element}
     */
    create: function(audio, sprites) {
        if(!this.loaded) {
            this.__declareElements();
            this.loaded = true;
        }
        var element = new this.audio_element(audio, sprites);
        element.__init();
        return element;
    },

    /**
     * Audio sprite object
     * @constructor
     * @param audio {object} Audio object with main audio file
     * @param sprites {object} Set of sprites.
     */
    audio_element: function(audio, sprites) {
        this.audio = audio;
        this.sprites = sprites;
        this.current = null;
        this.callback = null;
        this.loop = null;
    },

    /**
     * Declare element methods
     * @returns {boolean}
     * @private
     */
    __declareElements: function() {
        /**
         * Play sprite from audio file
         * @param sprite {string} Sprite name. If null, play random sprite
         * @param callback {function} Callback function at the end of the sprite
         * @param loop {bool} Loop current sprite
         * @returns {boolean}
         */
        as.audio_element.prototype.play = function (sprite, callback, loop) {
            try {
                this.callback = null;
                this.loop = null;

                // if we have not item, take random
                if (!sprite) {
                    var count = 0;
                    for (var prop in this.sprites) {
                        if (Math.random() < 1 / ++count)
                            sprite = prop;
                    }
                }
                // set current sprite for audio object
                this.current = this.sprites[sprite];
                this.audio.currentTime = this.current.start;
                // save callback argument
                if(callback) {
                    this.callback = callback
                }
                // save looping sprite
                if(loop) {
                    this.loop = sprite;
                }
                this.audio.play();
            }
            catch (e) {
                console.error('Audio sprites error: ' + e);
                return false;
            }
        };

        /**
         * Stop current sprite playing
         */
        as.audio_element.prototype.stop = function() {
            this.callback = null;
            this.loop = null;
            this.audio.pause();
        };

        /**
         * Pause current sprite
         * @private
         */
        this.audio_element.prototype.__pause = function () {
            if (this.current && this.audio.currentTime >= this.current.stop) {
                this.audio.pause();
                // run sprite callback if exist
                if(this.callback) {
                    this.callback();
                }
                // play sprite again if it looped
                if(this.loop) {
                    this.play(this.loop, this.callback, true);
                }
            }
        };
        /**
         * Init events for audio element
         * @private
         */
        this.audio_element.prototype.__init = function () {
            this.audio.addEventListener('timeupdate', (function (elem) {
                return function (e) {
                    elem.__pause();
                }
            })(this), false);
        };
    }
}