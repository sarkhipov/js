/**
 * Presentation gallery plugin
 */

(function ($) {

    'use strict';

    var defaultOptions = {
        transition: 'fade', // 'fade', 'none',
        keyboard: true,
        showPanel: true,
        defaultSlide: 0,
        slideShow: true,
        slideShowInterval: 3 // in seconds
    };

    /**
     * @param elem
     * @param options
     * @constructor
     */
    var PGallery = function(elem, options) {
        // extend от jquery почему то для новых объектов создает ссылку
        // надо разобраться, пока использую свой extend
        this.options = __extend(defaultOptions, options);
        this.elem = elem;
        this.container = $('<div></div>').addClass('p-gallery');
        this.currentSlide = null;
        this.currentSlideIndex = null;
        this.slides = [];
        this.slideShowTimer = null;
        this.init();
    };

    /**
     *
     */
    PGallery.prototype.init = function() {
        var self = this;
        this.elem.addClass('p-gallery-wrapper');
        this.elem.children().each(function() {
            self.createSlide($(this));
        });
        this.container.appendTo(this.elem);

        if(this.options.showPanel) {
            this.showPanel();
        }

        this.showSlide(this.options.defaultSlide);

        $(document).bind('keyup', function(e) {
            if(!self.options.keyboard) {
                return false;
            }
            var code = e.keyCode || e.which;
            if(code == 39) {
                self.nextSlide();
            }
            if(code == 37) {
                self.prevSlide();
            }
        })
    };

    /**
     * @param content
     */
    PGallery.prototype.createSlide = function(content) {
        var slide = $('<div></div>')
            .addClass('slide')
            .html(content)
            .appendTo(this.container);

        // обработка слайда в зависимости от типа его содержимого
        // пока выделяются только картинки, но это может быть видео, аудио и прочее
        var type = content.get(0).tagName.toLowerCase();
        if(type === 'img') {
            slide.addClass('slide-img');
        }
        // остальное содержимое считаем текстом и просто задаем обертку
        else {
            content = slide.html();
            slide.addClass('slide-text-wrapper').empty();
            $('<div></div>').addClass('slide-text')
                .html(content).appendTo(slide);
        }

        this.slides.push(slide);
    };

    /**
     * @param index
     */
    PGallery.prototype.showSlide = function(index) {
        var slide = this.getSlide(index);
        if(!slide) {
            return false;
        }

        // обработка переходов между слайдами
        // сюда можно добавить еще несколько вариантов смены слайда
        if(this.options.transition === 'fade') {
            if(this.currentSlide) {
                this.currentSlide.fadeOut(300);
            }
            slide.fadeIn(300);
        }
        else {
            if(this.currentSlide) {
                this.currentSlide.hide();
            }
            slide.show();
        }

        this.currentSlide = slide;
        this.currentSlideIndex = index*1;

        this.container.find('.prev, .next, .nav span').removeClass('disabled');
        if(index == 0) {
            this.container.find('.prev').addClass('disabled');
        }
        if(index == (this.slides.length - 1)) {
            this.container.find('.next').addClass('disabled');
        }
        this.container.find('.nav span.nav-slide-'+index).addClass('disabled');

        this.container.find('.number-caption').html((index*1+1)+'/'+this.slides.length);

        return true;
    };

    /**
     *
     */
    PGallery.prototype.nextSlide = function() {
        if(this.currentSlideIndex === null) {
            return false;
        }
        return this.showSlide(this.currentSlideIndex + 1);
    };

    /**
     *
     */
    PGallery.prototype.prevSlide = function() {
        if(this.currentSlideIndex === null)  {
            return false;
        }
        return this.showSlide(this.currentSlideIndex - 1);
    };

    /**
     * @param index
     * @returns {*}
     */
    PGallery.prototype.getSlide = function(index) {
        index = parseInt(index);
        if(index < 0 || index  > (this.slides.length-1)) {
            return false;
        }
        return this.slides[index];
    };

    /**
     *
     */
    PGallery.prototype.showPanel = function() {

        var panel = $('<div></div>').addClass('panel');

        $('<span></span>').addClass('number-caption').appendTo(panel);
        $('<span></span>').addClass('prev').appendTo(panel);
        var nav = $('<span></span>').addClass('nav').appendTo(panel);
        for(var k in this.slides) {
            $('<span></span>').addClass('nav-slide-'+k).data('index', k).appendTo(nav);
        }
        $('<span></span>').addClass('next').appendTo(panel);

        if(this.options.slideShow) {
            $('<span></span>').addClass('play').appendTo(panel);
        }

        panel.appendTo(this.container);
        this.container.height(this.container.height() - panel.height());

        if(this.slides.length < 2) {
            this.container.find('.play').addClass('disabled');
        }

        var self = this;
        this.container.find('.nav span').bind('click', function() {
            if($(this).hasClass('disabled')) {
                return false;
            }
            self.showSlide($(this).data('index'));
        });

        this.container.find('.prev').bind('click', function() {
            if($(this).hasClass('disabled')) {
                return false;
            }
            self.prevSlide();
        });

        this.container.find('.next').bind('click', function() {
            if($(this).hasClass('disabled')) {
                return false;
            }
            self.nextSlide();
        });

        this.container.find('.play').bind('click', function() {
            if($(this).hasClass('disabled')) {
                return false;
            }
            if($(this).hasClass('pause')) {
                self.stopSlideShow();
            }
            else {
                self.startSlideShow();
            }
        });
    };

    /**
     *
     */
    PGallery.prototype.startSlideShow = function() {
        this.container.find('.play').addClass('pause');
        this.slideShowTimer = setInterval(function(self) {
            if(!self.nextSlide()) {
                self.stopSlideShow();
            }
        }, this.options.slideShowInterval * 1000, this);
    };

    /**
     *
     */
    PGallery.prototype.stopSlideShow = function() {
        this.container.find('.play').removeClass('pause');
        clearInterval(this.slideShowTimer);
    };

    /**
     * Custom extend
     * @param obj1
     * @param obj2
     * @returns {{}}
     * @private
     */
    function __extend(obj1,obj2){
        var obj3 = {};
        for (var a in obj1) { obj3[a] = obj1[a]; }
        for (var a in obj2) { obj3[a] = obj2[a]; }
        return obj3;
    };


    /**
     * Init JQuery plugin
     *
     * @param param
     * @returns {*}
     */
    $.fn.pGallery = function (param) {
        return this.each(function () {
            var self = $(this);
            var data = self.data('pgallery');
            if (!data) {
                var options = (typeof param === 'object')?param:{};
                self.data('pgallery', (data = new PGallery(self, options)));
            }
            if (typeof param === 'string' && data[param]) {
                data[param]();
            }
        });
    };
})(jQuery);
