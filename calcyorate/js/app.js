'use strict';

var app = (function() {

    var activity = [];

    /**
     *
     */
    function bind() {
        $('.btn-gender').click(function() {
            app.setGender($(this));
        });

        $('.btn-target').click(function() {
            app.setTarget($(this));
        });

        $('.activity-block > div').mouseover(function() {
            app.hoverActivity($(this));
        }).mouseleave(function() {
            $('.activity-block > div').removeClass('hover');
        }).click(function() {
            app.setActivity($(this));
        });

        $('.range').bind('change input', function() {
            app.setRange($(this));
        });
    }

    /**
     *
     */
    function getData() {
        $.getJSON('activity.json', function(r) {
            $('.loader').hide();
            activity = r;
            collectData();
            $('.content').fadeIn(300);
        });
    }

    /**
     *
     */
    function collectData() {
        var data = {
            gender: $('.btn-gender.active').data('gender'),
            age: $('.age').val(),
            height: $('.height').val(),
            weight: $('.weight').val(),
            activity: $('.activity-block > div.active').last().data('activity'),
            target: $('.btn-target.active').data('target')
        };
        if(!validate(data)) {
            return false;
        }
        calculate(data);
    }

    /**
     * @param data
     */
    function validate(data) {
        // здесь можно дополнительно валидировать значения, если не поддерживается тип поля range
        return true;
    }

    /**
     * @param data
     */
    function calculate(data) {
        var result = 0;
        if(data.gender === 'man') {
            result = 66.47 + 13.75 * data.weight + 5 * data.height - 6.74 * data.age;
        }
        else {
            result = 655.1 + 9.6 * data.weight + 1.85 * data.height - 4.68 * data.age;
        }

        result *= activity[data.activity - 1].value;

        if(data.target === 'loss') {
            result -= result * 0.15;
        }

        drawProfile(data, parseInt(result));
    }

    /**
     * @param data
     * @param result
     */
    function drawProfile(data, result) {
        $('.profile-image').removeClass('woman').removeClass('man').addClass(data.gender);
        $('.summary-activity').html(activity[data.activity - 1].title);

        $('.summary-age').html(data.age + ' ' + getWord(data.age, 'год', 'года', 'лет'));
        $('.summary-height').html(data.height + ' ' + getWord(data.height, 'сантиметр', 'сантиметра', 'сантиметров'));
        $('.summary-weight').html(data.weight + ' ' + getWord(data.weight, 'килограмм', 'килограмма', 'килограмм'));

        var text = 'Для '+ ((data.target === 'normal') ? 'поддержания формы': 'похудения') + ' необходимо потреблять в день около';
        $('.result-title').html(text);
        $('.result').html(result + ' Ккал');
    }

    /**
     * @param num
     * @param w1
     * @param w2
     * @param w3
     */
    function getWord(num, w1, w2, w3) {
        var val = num % 100;
        if (val > 10 && val < 20) {
            return w1;
        }
        else {
            val = num % 10;
            if (val == 1) {
                return w1;
            }
            else if (val > 1 && val < 5) {
                return w2
            }
            else {
                return w3;
            }
        }
    }

    //
    return {

        init: function() {
            bind();
            getData();
        },

        /**
         * @param elem
         */
        setGender: function(elem) {
            $('.btn-gender').removeClass('active');
            elem.addClass('active');
            collectData();
        },

        /**
         * @param elem
         */
        setTarget: function(elem) {
            $('.btn-target').removeClass('active');
            elem.addClass('active');
            collectData();
        },

        /**
         * @param elem
         */
        setActivity: function(elem) {
            $('.activity-block > div').removeClass('active');
            $('.activity-block > div:nth-child(-n+'+elem.data('activity')+')').addClass('active');
            collectData();
        },

        /**
         * @param elem
         */
        hoverActivity: function(elem) {
            $('.activity-block > div:nth-child(-n+'+elem.data('activity')+')').addClass('hover');
        },

        /**
         * @param elem
         */
        setRange: function(elem) {
            collectData();
        }
    }
})();

$(document).ready(function() {
    app.init();
});